package com.gnomo.kuma.controller;

import com.gnomo.kuma.controller.exception.KumaPositionInfoNotFound;
import com.gnomo.kuma.dto.KumaPositionInfoDto;
import com.gnomo.kuma.entity.KumaPositionInfoEntity;
import com.gnomo.kuma.service.KumaPositionInfoService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class KumaPositionControllerTest {
    @Mock
    private KumaPositionInfoService kumaPositionInfoService;
    @InjectMocks
    private KumaPositionController sut;

    @Test
    public void shouldReturnPositionInfoWhenIdProvided() throws KumaPositionInfoNotFound {
        final int KUMA_ID = 0;

        KumaPositionInfoEntity kumaPositionInfoEntity = KumaPositionInfoEntity.builder().id(KUMA_ID).build();
        when(kumaPositionInfoService.getKumaPositionById(KUMA_ID)).thenReturn(kumaPositionInfoEntity);
        KumaPositionInfoDto result  = sut.getKumaPositionInfoById(KUMA_ID);

        assertNotNull(result);
        assertEquals(KUMA_ID, result.getId());
    }

    @Test
    public void shouldThrowExceptionWhenKumaPositionInfoNotFound() {
        final int KUMA_ID = 0;
        assertThrows(KumaPositionInfoNotFound.class, () -> sut.getKumaPositionInfoById(KUMA_ID));
    }

    @Test
    public void shouldUpdateKumaPositionForId() {
        final int KUMA_ID = 0;
        final double LAT = 0;
        final double LON = 0;
        final String LINK = "link";

        KumaPositionInfoDto kumaPositionInfoDTO = KumaPositionInfoDto.builder()
                .id(KUMA_ID).lat(LAT).lon(LON).link(LINK).build();
        when(kumaPositionInfoService.updateKumaPosition(KUMA_ID, LAT, LON, LINK)).thenReturn(KumaPositionInfoEntity.builder().build());

        KumaPositionInfoDto result =  sut.updateKumaPosition(kumaPositionInfoDTO);

        verify(kumaPositionInfoService, times(1)).updateKumaPosition(KUMA_ID, LAT, LON, LINK);
        assertNotNull(result);
    }
}
