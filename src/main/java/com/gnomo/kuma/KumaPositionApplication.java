package com.gnomo.kuma;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class KumaPositionApplication {

	public static void main(String[] args) {
		SpringApplication.run(KumaPositionApplication.class, args);
	}
}
