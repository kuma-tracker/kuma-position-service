package com.gnomo.kuma.repository;

import com.gnomo.kuma.entity.KumaPositionTestInfoEntity;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.data.repository.CrudRepository;

@Configuration
public interface KumaInfoRepository extends CrudRepository<KumaPositionTestInfoEntity, Integer> {
}
