package com.gnomo.kuma;

import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@CucumberContextConfiguration
@SpringBootTest()
@EnableCassandraRepositories(basePackages = "com.gnomo.kuma.repository")
public class CucumberSpringConfiguration {

}