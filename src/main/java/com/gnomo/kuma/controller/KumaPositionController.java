package com.gnomo.kuma.controller;

import com.gnomo.kuma.controller.exception.KumaPositionInfoNotFound;
import com.gnomo.kuma.dto.KumaPositionInfoDto;
import com.gnomo.kuma.entity.KumaPositionInfoEntity;
import com.gnomo.kuma.service.KumaPositionInfoService;
import org.springframework.web.bind.annotation.*;

@RestController
public class KumaPositionController {

    private final KumaPositionInfoService kumaPositionInfoService;

    public KumaPositionController(KumaPositionInfoService kumaPositionInfoService) {
        this.kumaPositionInfoService = kumaPositionInfoService;
    }

    @GetMapping("/kuma/position/{kumaId}")
    public KumaPositionInfoDto getKumaPositionInfoById(@PathVariable int kumaId) throws KumaPositionInfoNotFound {
        KumaPositionInfoEntity kumaPositionInfoEntity = kumaPositionInfoService.getKumaPositionById(kumaId);
        if(kumaPositionInfoEntity == null){
            throw new KumaPositionInfoNotFound();
        }
        return convertKumaEntityToDto(kumaPositionInfoEntity);
    }

    @PutMapping("/kuma/position")
    public KumaPositionInfoDto updateKumaPosition(KumaPositionInfoDto kumaPositionDTO) {

        KumaPositionInfoEntity result = kumaPositionInfoService.updateKumaPosition(
                kumaPositionDTO.getId(),
                kumaPositionDTO.getLat(),
                kumaPositionDTO.getLon(),
                kumaPositionDTO.getLink());

        return convertKumaEntityToDto(result);
    }

    private KumaPositionInfoDto convertKumaEntityToDto(KumaPositionInfoEntity kumaPositionInfoEntity) {
        return KumaPositionInfoDto.builder()
                .id(kumaPositionInfoEntity.getId())
                .lat(kumaPositionInfoEntity.getLat())
                .lon(kumaPositionInfoEntity.getLon())
                .link(kumaPositionInfoEntity.getLink())
                .build();
    }
}
