package com.gnomo.kuma.service.impl;

import com.gnomo.kuma.entity.KumaPositionInfoEntity;
import com.gnomo.kuma.repository.KumaPositionInfoRepository;
import com.gnomo.kuma.service.KumaPositionInfoService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class KumaPositionInfoServceImpl implements KumaPositionInfoService {

    private final KumaPositionInfoRepository kumaPositionInfoRepository;

    public KumaPositionInfoServceImpl(KumaPositionInfoRepository kumaPositionInfoRepository) {
        this.kumaPositionInfoRepository = kumaPositionInfoRepository;
    }

    @Override
    public KumaPositionInfoEntity getKumaPositionById(int kumaId) {
        Optional<KumaPositionInfoEntity> kumaOptional = kumaPositionInfoRepository.findById(kumaId);
        return kumaOptional.orElse(null);
    }

    @Override
    public KumaPositionInfoEntity updateKumaPosition(int kumaId, double lat, double lon, String link) {
        KumaPositionInfoEntity entity = KumaPositionInfoEntity.builder()
                .id(kumaId).lat(lat).lon(lon).link(link).build();
        return kumaPositionInfoRepository.save(entity);
    }


}
