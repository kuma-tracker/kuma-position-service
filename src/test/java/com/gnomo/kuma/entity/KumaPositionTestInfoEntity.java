package com.gnomo.kuma.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@Builder
@Table("position")
public class KumaPositionTestInfoEntity {
    @Id
    private int id;
    private double lat;
    private double lon;
}

