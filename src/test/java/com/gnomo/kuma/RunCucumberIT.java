package com.gnomo.kuma;

import org.junit.platform.suite.api.*;

@Suite
@SuiteDisplayName("JUnit Platform Suite Demo")
@IncludeEngines("cucumber")
public class RunCucumberIT {
}