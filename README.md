### Kuma Position Info Service

A REST Controller that provides location information for a given Kuma ID.
