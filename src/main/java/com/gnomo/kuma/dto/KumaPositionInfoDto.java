package com.gnomo.kuma.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class KumaPositionInfoDto {
    private int id;
    private double lat;
    private double lon;
    private String link;
}
