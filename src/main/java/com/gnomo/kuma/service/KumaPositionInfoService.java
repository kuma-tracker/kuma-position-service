package com.gnomo.kuma.service;

import com.gnomo.kuma.entity.KumaPositionInfoEntity;
import org.springframework.stereotype.Service;

@Service
public interface KumaPositionInfoService {
    KumaPositionInfoEntity getKumaPositionById(int kumaId);

    KumaPositionInfoEntity updateKumaPosition(int kumaId, double lat, double lon, String link);
}
