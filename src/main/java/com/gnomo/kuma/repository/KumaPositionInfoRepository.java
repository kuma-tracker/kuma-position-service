package com.gnomo.kuma.repository;

import com.gnomo.kuma.entity.KumaPositionInfoEntity;

import org.springframework.data.cassandra.repository.CassandraRepository;

public interface KumaPositionInfoRepository extends CassandraRepository<KumaPositionInfoEntity, Integer> {
}
