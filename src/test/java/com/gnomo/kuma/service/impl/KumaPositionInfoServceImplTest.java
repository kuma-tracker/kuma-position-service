package com.gnomo.kuma.service.impl;

import com.gnomo.kuma.dto.KumaPositionInfoDto;
import com.gnomo.kuma.entity.KumaPositionInfoEntity;
import com.gnomo.kuma.repository.KumaPositionInfoRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KumaPositionInfoServceImplTest {
    @Mock
    private KumaPositionInfoRepository kumaPositionInfoRepository;
    @InjectMocks
    private KumaPositionInfoServceImpl sut;

    @Test
    public void shouldGetKumaPositionInfoFromRespositoryWhenValidIdProvided()
    {
        final int KUMA_ID = 0;
        KumaPositionInfoEntity entity = KumaPositionInfoEntity.builder().id(KUMA_ID).build();
        when(kumaPositionInfoRepository.findById(KUMA_ID)).thenReturn(Optional.of(entity));
        KumaPositionInfoEntity resultEntity = sut.getKumaPositionById(KUMA_ID);
        assertNotNull(resultEntity);
    }

    @Test
    public void shouldReturnNullWhenKumaPositionDoesntExistForId()
    {
        final int INCORRECT_KUMA_ID = 1;
        KumaPositionInfoEntity resultEntity = sut.getKumaPositionById(INCORRECT_KUMA_ID);
        assertNull(resultEntity);
    }

    @Test
    public void shouldUpdateKumaPosition()
    {
        final int KUMA_ID = 0;
        final double LAT = 0;
        final double LON = 0;
        final String LINK = "link";

        KumaPositionInfoEntity kumaPositionInfoEntity = KumaPositionInfoEntity.builder()
                .id(KUMA_ID).lat(LAT).lon(LON).link(LINK).build();

        when(kumaPositionInfoRepository.save(kumaPositionInfoEntity)).thenReturn(kumaPositionInfoEntity);
        KumaPositionInfoEntity resultEntity = sut.updateKumaPosition(KUMA_ID, LAT, LON, LINK);
        assertNotNull(resultEntity);
    }
}